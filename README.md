# G-Core
Python 3.5.6

pip3 install -r requirements.txt
python3 manage.py makemigrations users
python3 manage.py makemigrations locations
python3 manage.py makemigrations visits

python3 manage.py test
python3 manage.py runserver 8000