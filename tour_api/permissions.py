import jwt
from rest_framework.permissions import BasePermission
from tour_api import settings


class IsOwner(BasePermission):
    def has_permission(self, request, view):
        token = request.META.get("HTTP_AUTHORIZATION")
        if not token:
            return False
        tmp = token.split(' ')
        if len(tmp) < 2 or len(tmp) >= 2 and tmp[1]:
            token = tmp[1]
            try:
                token_dec = jwt.decode(token, settings.JWT_AUTH["JWT_SECRET_KEY"])
                return token_dec.get("user_id") == request.user.id
            except:
                return False
        else:
            return False

