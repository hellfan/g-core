from django.conf.urls import url, include

urlpatterns = [
    url(r'^', include(('users.urls', 'users'), namespace='users')),
    url(r'^', include(('locations.urls', 'locations'), namespace='locations')),
    url(r'^', include(('visits.urls', 'visits'), namespace='visits')),
]
