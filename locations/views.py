from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from locations.models import Location
from locations.serializers import LocationSerializer
from tour_api.permissions import IsOwner
from visits.models import Visit
from users.serializers import UserSerializer


class LocationMixin(object):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer


class LocationRetrieveUpdateDestroyAPIView(LocationMixin, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated, IsOwner]


class LocationListCreateAPIView(LocationMixin, generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsOwner)


class LocationRateListAPIView(LocationMixin, generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def get(self, request, *args, **kwargs):
        location_id = kwargs.get("pk")
        filtered = Visit.objects.all().filter(location_id=location_id)
        if not filtered:
            return Response([], status=status.HTTP_200_OK)

        total = len(filtered)
        avg = sum([row.ratio for row in filtered])/total
        users = set(row.user_id for row in filtered)
        user_serializer = UserSerializer(users, many=True)

        result = {
            "count": total,
            "avg": avg,
            "visitors": user_serializer.data
        }

        return Response(result, status=status.HTTP_200_OK)
