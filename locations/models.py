from django.db import models
from django.utils import timezone


class Location(models.Model):
    class Meta:
        unique_together = ("country", "city", "name")

    id = models.BigAutoField(primary_key=True, unique=True)
    country = models.CharField(max_length=50, blank=False)
    city = models.CharField(max_length=50, blank=False)
    name = models.CharField(max_length=30, blank=False)
    description = models.CharField(max_length=2000, blank=False)
    create_date = models.DateTimeField(default=timezone.now)

    REQUIRED_FIELDS = ['country', 'city', 'name', 'description']
