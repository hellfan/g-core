from django.conf.urls import url

from locations.views import LocationRetrieveUpdateDestroyAPIView, LocationListCreateAPIView, LocationRateListAPIView
from visits.views import VisitInsertAPIView

urlpatterns = [
    url(r'^locations/(?P<pk>[\d]+)/visit$', VisitInsertAPIView.as_view(), name='location-visit'),
    url(r'^locations/(?P<pk>[\d]+)/ratio$', LocationRateListAPIView.as_view(), name='location-rate'),
    url(r'^locations/(?P<pk>[\d]+)$', LocationRetrieveUpdateDestroyAPIView.as_view(), name='location-operate'),
    url(r'^locations$', LocationListCreateAPIView.as_view(), name='location-list'),
]
