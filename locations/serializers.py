from rest_framework import serializers
from .models import Location


class LocationSerializer(serializers.ModelSerializer):
    create_date = serializers.ReadOnlyField()

    class Meta(object):
        model = Location
        fields = ('id', 'country', 'city', 'name', 'description', 'create_date')
