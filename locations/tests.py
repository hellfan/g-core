import json
from django.urls import reverse
from rest_framework.test import APITestCase
from users.models import User
from visits.models import Visit
from .models import Location


class LocationListCreateAPIViewTestCase(APITestCase):
    url = reverse("locations:location-list")
    auth_url = reverse("users:sign_in")

    def setUp(self):
        self.username = "test"
        self.first_name = "test"
        self.last_name = "test"
        self.email = "test@mail.ru"
        self.user = User.objects.create_user(self.username, **dict(zip(["first_name", "last_name", "email"],
                                                                       [self.first_name, self.last_name, self.email])))
        response = self.client.post(self.auth_url, {"username": self.username})
        data = json.loads(str(response.content.decode("utf-8")))
        self.token = data["token"]
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def tearDown(self):
        self.user.delete()

    def test_create(self):
        location_data = {
            "country": "Россия",
            "city": "Пермь",
            "name": "Парк 1",
            "description": "Парк Парк"
        }

        response = self.client.post(self.url, location_data)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(201, response.status_code)
        self.assertIsInstance(data, dict)
        self.assertIn("country", data)
        self.assertEqual(location_data["country"], data.get("country"))

    def test_create_unauthorized(self):
        location_data = {
            "country": "Россия",
            "city": "Пермь",
            "name": "Парк 1",
            "description": "Парк Парк"
        }
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token + "123")
        response = self.client.post(self.url, location_data)
        self.assertEqual(401, response.status_code)

    def test_create_duplicate(self):
        location_data = {
            "country": "Россия",
            "city": "Пермь",
            "name": "Парк 1",
            "description": "Парк Парк"
        }

        response = self.client.post(self.url, location_data)
        self.assertEqual(201, response.status_code)

        response = self.client.post(self.url, location_data)
        self.assertEqual(400, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("non_field_errors", data)

    def test_get_list(self):
        location = Location.objects.create(country='Россия', city='Пермь', name='Парк 1', description='Парк Парк')
        response = self.client.get(self.url)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(data, list)
        self.assertTrue(len(data) == 1)
        self.assertIsInstance(data[0], dict)
        self.assertIn('country', data[0])
        self.assertEqual(location.country, data[0].get("country"))
        location.delete()


class LocationRetrieveUpdateDestroyAPIViewTestCase(APITestCase):
    auth_url = reverse("users:sign_in")

    def setUp(self):
        self.username = "test"
        self.first_name = "test"
        self.last_name = "test"
        self.email = "test@mail.ru"
        self.user = User.objects.create_user(self.username, **dict(zip(["first_name", "last_name", "email"],
                                                                       [self.first_name, self.last_name, self.email])))
        response = self.client.post(self.auth_url, {"username": self.username})
        data = json.loads(str(response.content.decode("utf-8")))
        self.token = data["token"]
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        self.country = 'Россия'
        self.city = 'Пермь'
        self.name = 'Парк 1'
        self.description = 'Парк Парк'
        self.location = Location.objects.create(country=self.country, city=self.city, name=self.name,
                                                description=self.description)
        self.url = reverse("locations:location-operate", kwargs={"pk": self.location.id})
        self.visit_url = reverse("locations:location-visit", kwargs={"pk": self.location.id})
        self.ratio_url = reverse("locations:location-rate", kwargs={"pk": self.location.id})

    def tearDown(self):
        self.user.delete()
        self.location.delete()

    def test_get_by_key(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("city", data)
        self.assertEqual(data["city"], self.city)

    def test_delete_by_key(self):
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)
        self.assertEqual(0, len(Location.objects.all()))

    def test_delete_by_key_unauthorized(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token + "123")
        response = self.client.delete(self.url)
        self.assertEqual(401, response.status_code)

    def test_delete_by_absent_key(self):
        response = self.client.delete(reverse("locations:location-operate", kwargs={"pk": 99999}))
        self.assertEqual(404, response.status_code)

    def test_update_by_key(self):
        location_data = {
            "country": "Россия1",
            "city": "Пермь",
            "name": "Парк 1",
            "description": "Парк Парк"
        }
        response = self.client.put(self.url, location_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("country", data)
        self.assertEqual(data["country"], location_data["country"])

    def test_partial_update_by_key(self):
        location_data = {
            "city": "Пермь1",
        }
        response = self.client.patch(self.url, location_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("city", data)
        self.assertEqual(location_data["city"], data["city"])

    def test_visit_location_wo_ratio(self):
        response = self.client.post(self.visit_url)
        self.assertEqual(400, response.status_code)

    def test_visit_location(self):
        ratio = 6
        response = self.client.post(self.visit_url, {"ratio": ratio})
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(Visit.objects.all()))
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("ratio", data)
        self.assertEqual(ratio, data["ratio"])

    def test_ratio_location(self):
        Visit.objects.create(user_id=self.user, location_id=self.location, ratio=5)
        Visit.objects.create(user_id=self.user, location_id=self.location, ratio=9)

        response = self.client.get(self.ratio_url)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))

        self.assertIsInstance(data, dict)
        self.assertIn("visitors", data)
        self.assertIsInstance(data["visitors"], list)
        self.assertEqual(1, len(data["visitors"]))
        visitor = data["visitors"][0]
        self.assertIsInstance(visitor, dict)
        self.assertIn("email", visitor)
        self.assertEqual(self.user.email, visitor["email"])
