from django.conf.urls import url

from visits.views import VisitListAPIView, VisitRetrieveUpdateDestroyAPIView

urlpatterns = [
    url(r'^visits/(?P<pk>[\d]+)$', VisitRetrieveUpdateDestroyAPIView.as_view(), name='visit-operate'),
    url(r'^visits', VisitListAPIView.as_view(), name='visit-list'),
]
