from rest_framework import serializers
from .models import Visit


class VisitSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()

    class Meta:
        model = Visit
        fields = ('id', 'user_id', 'location_id', 'date', 'ratio')
