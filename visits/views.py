from rest_framework import status, generics
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from tour_api.permissions import IsOwner
from visits.serializers import VisitSerializer
from visits.models import Visit


class VisitMixin(object):
    queryset = Visit.objects.all()
    serializer_class = VisitSerializer


class VisitRetrieveUpdateDestroyAPIView(VisitMixin, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsOwner)


class VisitListAPIView(VisitMixin, generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsOwner)


class VisitInsertAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = VisitSerializer

    def post(self, request, *args, **kwargs):
        serializer_data = request.data.copy()
        serializer_data["user_id"] = request.user.id
        serializer_data["location_id"] = kwargs.get("pk")
        serializer = VisitSerializer(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)
