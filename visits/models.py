from django.db import models
from django.utils import timezone
from rest_framework.compat import MaxValueValidator, MinValueValidator

from locations.models import Location
from users.models import User


class Visit(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    location_id = models.ForeignKey(Location, on_delete=models.CASCADE)

    date = models.DateTimeField(default=timezone.now, verbose_name="Дата посещения")
    ratio = models.IntegerField(blank=False, validators=[
        MaxValueValidator(10),
        MinValueValidator(0)
    ], verbose_name="Оценка пользователем")

    REQUIRED_FIELDS = ['user_id', 'location_id', 'ratio']
