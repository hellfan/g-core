import json
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APITestCase

from users.models import User
from locations.models import Location
from .models import Visit


class VisitListRetrieveUpdateDestroyAPIViewTestCase(APITestCase):
    list_url = reverse("visits:visit-list")
    auth_url = reverse("users:sign_in")

    def setUp(self):
        self.username = "test"
        self.first_name = "test"
        self.last_name = "test"
        self.email = "test@mail.ru"
        self.user = User.objects.create_user(self.username, **dict(zip(["first_name", "last_name", "email"],
                                                                       [self.first_name, self.last_name, self.email])))
        response = self.client.post(self.auth_url, {"username": self.username})
        data = json.loads(str(response.content.decode("utf-8")))
        self.token = data["token"]
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        self.country = 'Россия'
        self.city = 'Пермь'
        self.name = 'Парк 1'
        self.description = 'Парк Парк'
        self.location = Location.objects.create(country=self.country, city=self.city, name=self.name,
                                                description=self.description)
        self.visit = Visit.objects.create(user_id=self.user, location_id=self.location, ratio=5)

        self.operate_url = reverse("visits:visit-operate", kwargs={"pk": self.visit.id})

    def tearDown(self):
        self.user.delete()
        self.location.delete()
        self.visit.delete()

    def test_get_list(self):
        response = self.client.get(self.list_url)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(data, list)
        self.assertTrue(len(data) == 1)
        self.assertIsInstance(data[0], dict)
        self.assertIn('location_id', data[0])
        self.assertEqual(self.visit.location_id.id, data[0].get("location_id"))

    def test_get_by_key(self):
        response = self.client.get(self.operate_url)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("ratio", data)
        self.assertEqual(data["ratio"], self.visit.ratio)

    def test_delete_by_key(self):
        response = self.client.delete(self.operate_url)
        self.assertEqual(204, response.status_code)
        self.assertEqual(0, len(Visit.objects.all()))

    def test_delete_by_key_unauthorized(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token + "123")
        response = self.client.delete(self.operate_url)
        self.assertEqual(401, response.status_code)

    def test_delete_by_absent_key(self):
        response = self.client.delete(reverse("locations:location-operate", kwargs={"pk": 99999}))
        self.assertEqual(404, response.status_code)

    def test_update_by_key(self):
        visit_data = {
            "user_id": self.user.id,
            "location_id": self.location.id,
            "date": timezone.now,
            "ratio": 1
        }
        response = self.client.put(self.operate_url, visit_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("ratio", data)
        self.assertEqual(data["ratio"], visit_data["ratio"])

    def test_partial_update_by_key(self):
        visit_data = {
            "ratio": 6,
        }
        response = self.client.patch(self.operate_url, visit_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("ratio", data)
        self.assertEqual(visit_data["ratio"], data["ratio"])
