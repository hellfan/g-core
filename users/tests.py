import json
from django.urls import reverse
from rest_framework.test import APITestCase
from .models import User
from locations.models import Location
from visits.models import Visit


class UserRegistrationAPIViewTestCase(APITestCase):
    url = reverse("users:register")

    def test_user_registration(self):
        """
        Test to verify that a post call with user valid data
        """
        user_data = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
            "email": "test@mail.ru"
        }

        response = self.client.post(self.url, user_data)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(201, response.status_code)
        self.assertEqual(1, data.get("id"))
        self.assertEqual(1, len(User.objects.all()))

    def test_unique_username_validation(self):
        """
        Test to verify that a post call with already exists username
        """
        user_data = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
            "email": "test@mail.ru"
        }

        response = self.client.post(self.url, user_data)
        self.assertEqual(201, response.status_code)

        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_required_field_validation(self):
        """
        Test to verify that a post call with absent filed
        """
        user_data = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
        }

        response = self.client.post(self.url, user_data)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(400, response.status_code)
        self.assertTrue("email" in data)
        self.assertEqual(0, len(User.objects.all()))

    def test_empty_field_validation(self):
        """
        Test to verify that a post call with absent filed
        """
        user_data = {
            "username": "",
            "first_name": "test",
            "last_name": "test",
            "email": "test@mail.ru"
        }

        response = self.client.post(self.url, user_data)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(400, response.status_code)
        self.assertTrue("username" in data)


class UserLoginAPIViewTestCase(APITestCase):
    url = reverse("users:sign_in")

    def setUp(self):
        self.username = "test"
        self.first_name = "test"
        self.last_name = "test"
        self.email = "test@mail.ru"
        self.user = User.objects.create_user(self.username, **dict(zip(["first_name", "last_name", "email"],
                                                                       [self.first_name, self.last_name, self.email])))

    def tearDown(self):
        self.user.delete()

    def test_authentication_without_username(self):
        response = self.client.post(self.url, {})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_username(self):
        response = self.client.post(self.url, {"username": "test1"})
        self.assertEqual(403, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(self.url, {"username": self.username})
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(200, response.status_code)
        self.assertTrue("token" in data)


class UserAPIViewTestCase(APITestCase):
    url = reverse("users:user-list")
    auth_url = reverse("users:sign_in")

    def setUp(self):
        self.username = "test"
        self.first_name = "test"
        self.last_name = "test"
        self.email = "test@mail.ru"
        self.user = User.objects.create_user(self.username, **dict(zip(["first_name", "last_name", "email"],
                                                                       [self.first_name, self.last_name, self.email])))
        response = self.client.post(self.auth_url, {"username": self.username})
        data = json.loads(str(response.content.decode("utf-8")))
        self.token = data["token"]
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.operate_url = reverse("users:user-operate", kwargs={"pk": self.user.id})

    def tearDown(self):
        self.user.delete()

    def test_unauthorized_get(self):
        # Test that unauthorized access returns 401
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token + "123")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_get_list(self):
        response = self.client.get(self.url)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(data, list)
        self.assertTrue(len(data) == 1)
        self.assertIsInstance(data[0], dict)
        self.assertIn('username', data[0])
        self.assertEqual(self.username, data[0].get("username"))

    def test_get_by_key(self):
        response = self.client.get(self.operate_url)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("username", data)
        self.assertEqual(data["username"], self.username)

    def test_delete_by_key(self):
        response = self.client.delete(self.operate_url)
        self.assertEqual(204, response.status_code)
        self.assertEqual(0, len(User.objects.all()))
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_delete_by_key_unauthorized(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token + "123")
        response = self.client.delete(self.operate_url)
        self.assertEqual(401, response.status_code)

    def test_delete_by_absent_key(self):
        response = self.client.delete(reverse("users:user-operate", kwargs={"pk": 99999}))
        self.assertEqual(404, response.status_code)

    def test_update_by_key(self):
        user_data = {
            "username": "test1",
            "first_name": "test",
            "last_name": "test",
            "email": "test@mail.ru"
        }
        response = self.client.put(self.operate_url, user_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("username", data)
        self.assertEqual(user_data["username"], data["username"])

    def test_partial_update_by_key(self):
        user_data = {
            "gender": "M"
        }
        response = self.client.patch(self.operate_url, user_data)
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("gender", data)
        self.assertEqual(user_data["gender"], data["gender"])

    def test_visit_location(self):
        location1 = Location.objects.create(country='Россия', city='Пермь', name='Парк 1', description='Парк Парк')
        location2 = Location.objects.create(country='Россия', city='Пермь', name='Парк 2', description='Парк Парк')
        Visit.objects.create(user_id=self.user, location_id=location1, ratio=5)
        Visit.objects.create(user_id=self.user, location_id=location2, ratio=9)

        response = self.client.get(reverse("users:user-location", kwargs={"pk": location1.id}))
        self.assertEqual(200, response.status_code)
        data = json.loads(str(response.content.decode("utf-8")))
        self.assertIsInstance(data, dict)
        self.assertIn("locations", data)
        self.assertIsInstance(data["locations"], list)
        self.assertEqual(2, len(data["locations"]))
        location = data["locations"][0]
        self.assertIsInstance(location, dict)
        self.assertIn("country", location)
        self.assertEqual("Россия", location["country"])
        location1.delete()
        location2.delete()
