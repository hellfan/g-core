from django.db import models, transaction
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin,
    BaseUserManager)


class UserManager(BaseUserManager):

    def _create_user(self, username, **extra_fields):
        if not username:
            raise ValueError('The given username must be set')
        try:
            with transaction.atomic():
                user = self.model(username=username, **extra_fields)
                user.save()
                return user
        except:
            raise

    def create_user(self, username, **extra_fields):
        return self._create_user(username=username, **extra_fields)

    def create_superuser(self, username, **extra_fields):
        return self._create_user(username=username, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True, unique=True)
    username = models.CharField(max_length=30, blank=False, unique=True)
    email = models.EmailField(max_length=40, blank=False, unique=True)
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(default=timezone.now)

    GENDER_CHOICES = (
        ("M", "Male"),
        ("F", "Female")
    )

    gender = models.CharField(max_length=1, blank=True, choices=GENDER_CHOICES, null=True)
    birth_date = models.DateField(blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self
