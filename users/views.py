import jwt
from django.contrib.auth import user_logged_in
from rest_framework import status, generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.serializers import jwt_payload_handler

from tour_api.permissions import IsOwner
from .models import User
from .serializers import UserSerializer
from tour_api import settings
from visits.models import Visit
from locations.serializers import LocationSerializer


class CreateUserAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def sign_in(request):
    try:
        username = request.data['username']

        user = User.objects.get(username=username)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.JWT_AUTH["JWT_SECRET_KEY"])
                user_details = {
                    'name': "%s %s" % (user.first_name, user.last_name),
                    'user_id': str(user.id),
                    'token': token
                }
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return Response(user_details, status=status.HTTP_200_OK)

            except Exception as e:
                return Response({
                    'error': str(e)},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({
                'error': 'can not auth with the given credentials or the account has been deactivated'},
                status=status.HTTP_403_FORBIDDEN)
    except User.DoesNotExist:
        return Response({
            'error': 'can not auth with the given credentials or the account has been deactivated'},
            status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        return Response({'error': 'please provide a username'}, status=status.HTTP_400_BAD_REQUEST)


class UserMixin(object):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserRetrieveUpdateAPIView(UserMixin, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsOwner)


class UserViewSet(UserMixin, generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsOwner)


class UserRateListAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get("pk")
        filtered = Visit.objects.all().filter(user_id=user_id)
        if not filtered:
            return Response([], status=status.HTTP_200_OK)

        total = len(filtered)
        avg = sum([row.ratio for row in filtered]) / total
        locations = [row.location_id for row in filtered]
        location_serializer = LocationSerializer(locations, many=True)

        result = {
            "count": total,
            "avg": avg,
            "locations": location_serializer.data
        }

        return Response(result, status=status.HTTP_200_OK)
