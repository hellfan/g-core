from django.conf.urls import url
from .views import CreateUserAPIView, UserRetrieveUpdateAPIView, sign_in, UserViewSet, UserRateListAPIView

urlpatterns = [
    url(r'^register$', CreateUserAPIView.as_view(), name="register"),
    url(r'^sign_in$', sign_in, name="sign_in"),
    url(r'^users/(?P<pk>[\d]+)$', UserRetrieveUpdateAPIView.as_view(), name="user-operate"),
    url(r'^users/(?P<pk>[\d]+)/ratio$', UserRateListAPIView.as_view(), name='user-location'),
    url(r'^users$', UserViewSet.as_view(), name='user-list'),
]
