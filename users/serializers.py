from rest_framework import serializers

# from visits.serializers import VisitSerializer
from .models import User


class UserSerializer(serializers.ModelSerializer):
    create_date = serializers.ReadOnlyField()

    # visit_set = VisitSerializer(many=True)

    class Meta(object):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name',
                  'username', 'is_active', 'create_date',
                  'gender', 'birth_date', 'country')  # 'visit_set'
